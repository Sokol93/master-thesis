#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{
	TEST_CLASS(UTests_NNetwork)
	{
	public:

		// Sprawdzenie, czy prawidlowo zostanie wyliczona wartosc dwuwarstwowej sieci neuronowej o binarnej funkcji aktywacji
		TEST_METHOD(Process_ExampleNetworkBinaryActivationFunction_ExpectedOutput)
		{
			const unsigned int inputs = 3;
			const unsigned int outputs = 2;
			const unsigned int connections = inputs*outputs;

			NEAT_Network neat_network(3, 2, 0);
			neat_network.AddHiddenNodes(1);
			
			for (unsigned int i = 0; i < connections; ++i)
				neat_network.ChangeEnabledStatus(i, false);

			neat_network.AddConnection(NodesConnection(0, 5, -1, true, 0));
			neat_network.AddConnection(NodesConnection(1, 5, 1, true, 0));
			neat_network.AddConnection(NodesConnection(2, 4, 1, true, 0));
			neat_network.AddConnection(NodesConnection(5, 3, 1, true, 0));
			neat_network.AddConnection(NodesConnection(5, 4, -3, true, 0));

			NNetwork network = neat_network.ToNNetwork();
			network.ChangeActivationFunction(new BinaryFunction());

			std::vector<double> params({ 1, 2, 3 });

			std::list<double> expected({ 1, 0 });
			auto actual(network.Process(params));

			Assert::AreEqual(expected.size(), actual.size());
			Assert::AreEqual(expected.front(), actual.front());
			Assert::AreEqual(expected.back(), actual.back());
		}
		// Sprawdzenie, czy prawidlowo zostanie wyliczona wartosc dwuwarstwowej sieci neuronowej o sigmoidalnej funkcji aktywacji
		TEST_METHOD(Process_ExampleNetworkSigmoidalActivationFunction_ExpectedOutput)
		{
			const unsigned int inputs = 3;
			const unsigned int outputs = 2;
			const unsigned int connections = inputs*outputs;

			NEAT_Network neat_network(3, 2, 0);
			neat_network.AddHiddenNodes(1);

			for (unsigned int i = 0; i < connections; ++i)
				neat_network.ChangeEnabledStatus(i, false);

			neat_network.AddConnection(NodesConnection(0, 5, -1, true, 0));
			neat_network.AddConnection(NodesConnection(1, 5, 1, true, 0));
			neat_network.AddConnection(NodesConnection(2, 4, 1, true, 0));
			neat_network.AddConnection(NodesConnection(5, 3, 1, true, 0));
			neat_network.AddConnection(NodesConnection(5, 4, -3, true, 0));

			NNetwork network = neat_network.ToNNetwork();
			network.ChangeActivationFunction(new SigmoidalFunction(1));

			std::vector<double> params({ 1, 0, 1 });

			double hiddenNodeOutput = SigmoidalFunction(1).Calculate(1 * (-1) + 0 * 1);
			std::list<double> expected({ SigmoidalFunction(1).Calculate(hiddenNodeOutput * 1), SigmoidalFunction(1).Calculate(hiddenNodeOutput*(-3) + 1 * 1) });
			auto actual(network.Process(params));

			Assert::AreEqual(expected.size(), actual.size());
			Assert::AreEqual(expected.front(), actual.front());
			Assert::AreEqual(expected.back(), actual.back());
		}

	};
}