// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

// Headers for CppUnitTest
#include "CppUnitTest.h"
#include <list>
#include <vector>
#include <string>
#include "../Master_Thesis/Coordinates.h"
#include "../Master_Thesis/GameObject.h"
#include "../Master_Thesis/ActivationFunction.h"
#include "../Master_Thesis/BinaryFunction.h"
#include "../Master_Thesis/Neuron.h"
#include "../Master_Thesis/InputNeuron.h"
#include "../Master_Thesis/NEAT_Network.h"
#include "../Master_Thesis/HelperFunctions.h"
#include "../Master_Thesis/NNetwork.h"
