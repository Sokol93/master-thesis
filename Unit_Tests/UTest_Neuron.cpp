#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{
	TEST_CLASS(UTests_Neuron)
	{
	public:

		// Sprawdzenie, czy prawidlowo zostanie wyliczona wartosc dwuwarstwowej sieci neuronowej
		TEST_METHOD(GetValue_BinaryFunction_ExpectedOutput)
		{
			BinaryFunction binFunction;
			InputNeuron input1(1);
			InputNeuron input2(2);
			Neuron hiddenNeruon(std::vector<Neuron*>({ &input1, &input2 }), std::vector<double>({ 1, -1 }), &binFunction);
			Neuron output(std::vector<Neuron*>({ &hiddenNeruon, &input2 }), std::vector<double>({ -3, 2 }), &binFunction);

			double expected = 1;
			double actual = output.GetValue();

			Assert::AreEqual(expected, actual);
		}
		// Sprawdzenie, czy prawidlowo zostanie wyliczona wartosc dwuwarstwowej sieci neuronowej
		TEST_METHOD(GetValue_SigmoidalFunction_ExpectedOutput)
		{
			SigmoidalFunction sigFunction(1);
			InputNeuron input1(1);
			InputNeuron input2(0.1);
			Neuron hiddenNeruon(std::vector<Neuron*>({ &input1, &input2 }), std::vector<double>({ 1, -1 }), &sigFunction);
			Neuron output(std::vector<Neuron*>({ &hiddenNeruon, &input2 }), std::vector<double>({ -3, 2 }), &sigFunction);

			double hiddenOutput = sigFunction.Calculate(1 * 1 + 0.1*(-1));
			double expected = sigFunction.Calculate(hiddenOutput*(-3) + 0.1 * 2);
			double actual = output.GetValue();

			Assert::AreEqual(expected, actual);
		}
		// Sprawdzenie, czy prawidlowo zostanie wyrzucony wyjatek, gdy siec bedzie chciala wyliczyc wartosc wyjsciowa przy nieustawionych wejsciach
		TEST_METHOD(GetValue_NoInputsSet_ExpectedException)
		{
			BinaryFunction binFunction;
			InputNeuron input1;
			InputNeuron input2;
			Neuron hiddenNeruon(std::vector<Neuron*>({ &input1, &input2 }), std::vector<double>({ 1, -1 }), &binFunction);
			Neuron output(std::vector<Neuron*>({ &hiddenNeruon, &input2 }), std::vector<double>({ -3, 2 }), &binFunction);

			try
			{
				output.GetValue();
				throw 0;
			}
			catch (std::string) {}
		}
		// Sprawdzenie, czy prawidlowo zostanie wyliczona wartosc dwuwarstwowej sieci neuronowej
		TEST_METHOD(AddInput_EmptyNetwork_ExpectedOutput)
		{
			BinaryFunction binFunction;
			InputNeuron input1(1);
			InputNeuron input2(2);
			Neuron hiddenNeruon(&binFunction);
			hiddenNeruon.AddInput(&input1, 1);
			hiddenNeruon.AddInput(&input2, -1);
			Neuron output(&binFunction);
			output.AddInput(&hiddenNeruon, -3);
			output.AddInput(&input2, 2);

			double expected = 1;
			double actual = output.GetValue();

			Assert::AreEqual(expected, actual);
		}

	};
}