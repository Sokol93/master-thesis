#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{
	TEST_CLASS(UTests_InputNeuron)
	{
	public:

		// Sprawdzenie, czy rzucony zostanie wyjatek w momencie odwolania sie do wartosci neuronu wejsciowego bez wczesniejszej jej okreslenia
		TEST_METHOD(GetValue_NoValueSet_ExpectedException)
		{
			// Bez kroku
			InputNeuron neuron;

			bool exceptionCaught = false;

			try {
				neuron.GetValue();
			}
			catch (std::string)
			{
				exceptionCaught = true;
			}

			Assert::IsTrue(exceptionCaught);
		}

		// Sprawdzenie, czy prawidlowo przypisano wartosc neuronu poprzez konstruktor
		TEST_METHOD(GetValue_ValueSetInConstructor_SetValue)
		{
			// Bez kroku
			InputNeuron neuron(123);

			double actual = neuron.GetValue();
			double expected = 123;

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy prawidlowo przypisano wartosc neuronu poprzez metode klasy
		TEST_METHOD(GetValue_ValueSetByMethod_SetValue)
		{
			// Bez kroku
			InputNeuron neuron;

			neuron.SetValue(123);

			double actual = neuron.GetValue();
			double expected = 123;

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy zostanie rzucony wyjatek po zresetowaniu wartosci neuronu
		TEST_METHOD(GetValue_ValueSetAndReseted_ExpectedException)
		{
			// Bez kroku
			InputNeuron neuron(123);

			neuron.ResetValue();

			bool exceptionCaught = false;

			try {
				neuron.GetValue();
			}
			catch (std::string)
			{
				exceptionCaught = true;
			}

			Assert::IsTrue(exceptionCaught);
		}

	};
}