#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{
	TEST_CLASS(UTests_BinaryFunction)
	{
	public:

		// Sprawdzenie, czy wyliczona wartosc wyniesie 1, gdy nie ma przesuniecia i wartosc funkcji przekroczy granice
		TEST_METHOD(Calculate_NoTransAndAboveTreshold_ReturnedValueOne)
		{
			// Bez kroku
			BinaryFunction func;

			double expected = 1;
			double actual = func.Calculate(0.1);

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy wyliczona wartosc wyniesie 0, gdy nie ma przesuniecia i wartosc funkcji znajduje sie na granicy
		TEST_METHOD(Calculate_NoTransAndAtTreshold_ReturnedValueZero)
		{
			// Bez kroku
			BinaryFunction func;

			double expected = 0;
			double actual = func.Calculate(0);

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy wyliczona wartosc wyniesie 0, gdy nie ma przesuniecia i wartosc funkcji znajduje sie ponizej granicy
		TEST_METHOD(Calculate_NoTransAndBelowTreshold_ReturnedValueZero)
		{
			// Bez kroku
			BinaryFunction func;

			double expected = 0;
			double actual = func.Calculate(-0.1);

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy wyliczona wartosc wyniesie 1, gdy funkcja jest przesunieta i wartosc funkcji przekroczy granice
		TEST_METHOD(Calculate_WithTransAndAboveTreshold_ReturnedValueOne)
		{
			// Bez kroku
			BinaryFunction func(1);

			double expected = 1;
			double actual = func.Calculate(1.1);

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy wyliczona wartosc wyniesie 0, gdy funkcja jest przesunieta i wartosc funkcji znajduje sie na granicy
		TEST_METHOD(Calculate_WithTransAndAtTreshold_ReturnedValueZero)
		{
			// Bez kroku
			BinaryFunction func(1);

			double expected = 0;
			double actual = func.Calculate(1);

			Assert::AreEqual(expected, actual);
		}

		// Sprawdzenie, czy wyliczona wartosc wyniesie 0, gdy funkcja jest przesunieta i wartosc funkcji znajduje sie ponizej granicy
		TEST_METHOD(Calculate_WithTransAndBelowTreshold_ReturnedValueZero)
		{
			// Bez kroku
			BinaryFunction func(1);

			double expected = 0;
			double actual = func.Calculate(-1.1);

			Assert::AreEqual(expected, actual);
		}

	};
}