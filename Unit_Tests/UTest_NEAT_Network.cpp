#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{
	TEST_CLASS(UTests_NEAT_Network)
	{
	public:

		// Sprawdzenie, czy wagi prawidlowo zostana zmutowane (ich wartosci sie zmienia
		TEST_METHOD(MutateWeights_OryginalAndMutatedNetwork_OryginalNetworkNotChangedAndMutatedNetworkChanged)
		{
			NEAT_Network neat(3, 1, 90);

			NEAT_Network oryginal = neat;

			NEAT_Network mutated = neat.MutateWeights();

			auto oryginalConnections = oryginal.GetNodesConnections();
			auto mutatedConnections = mutated.GetNodesConnections();

			if (oryginalConnections[0] == mutatedConnections[0])
				throw "Elements are equal";
			if (oryginalConnections[1] == mutatedConnections[1])
				throw "Elements are equal";
		}
		// Sprawdzenie, czy prawidlowo zostaje dodany nowy wezel
		TEST_METHOD(Mutate_AddNode_OneInputAndOneOutputNetwork_AddedNewNode)
		{
			// Petla ma zapobiec przypadkowemu wylosowaniu oczekiwanego polaczenia (istnieja tylko 2)
			for (int i = 0; i < 50; i++) 
			{
				NEAT_Network oryginal(2, 1, 90);

				oryginal.ChangeEnabledStatus(0, false);

				NEAT_Network mutated = oryginal.Mutate_AddNode();

				auto oryginalConnections = oryginal.GetNodesConnections();
				auto mutatedConnections = mutated.GetNodesConnections();
				auto lastOryginalConnection = oryginalConnections.back();
				auto lastMutatedConnection = mutatedConnections.back();
				auto secondToLastMutatedConnection = mutatedConnections[mutatedConnections.size() - 2];

				Assert::AreEqual(false, mutatedConnections[1].isEnabled);
				Assert::AreEqual(oryginal.GetNeuronsNumber() + 1, mutated.GetNeuronsNumber());
				Assert::AreEqual(oryginalConnections.size() + 2, mutatedConnections.size());
				Assert::IsTrue(secondToLastMutatedConnection.in == lastOryginalConnection.in && secondToLastMutatedConnection.out != lastOryginalConnection.out);
				Assert::IsTrue(lastMutatedConnection.in != lastOryginalConnection.in && lastMutatedConnection.out == lastOryginalConnection.out);
			}
		}
		// Sprawdzenie, czy w przypadku gdy nie ma miejsca na nowe polaczenie, nie zostanie ono dodane
		TEST_METHOD(Mutate_AddConnection_NoFreeSpaceForNewConnection_NoConnectionAdded)
		{
			NEAT_Network oryginal(2, 2, 90);

			NEAT_Network mutated = oryginal.Mutate_AddConnection();

			auto oryginalConnections = oryginal.GetNodesConnections();
			auto mutatedConnections = mutated.GetNodesConnections();

			Assert::AreEqual(oryginalConnections.size(), mutatedConnections.size());
		}
		// Sprawdzenie, czy w przypadku gdy istnieje miejsce na nowe polaczenie, to prawidlowo zostanie ono dodane
		TEST_METHOD(Mutate_AddConnection_FreeSpaceForOneConnection_ConnectionCreated)
		{
			for (int i = 0; i < 50; i++)
			{
				NEAT_Network oryginal = NEAT_Network(2, 1, 90).Mutate_AddNode();
				NEAT_Network mutated = oryginal.Mutate_AddConnection();

				auto oryginalConnections = oryginal.GetNodesConnections();
				auto mutatedConnections = mutated.GetNodesConnections();
				auto lastConnection = mutatedConnections.back();

				Assert::AreNotEqual(oryginalConnections.size(), mutatedConnections.size());
				Assert::IsTrue((lastConnection.in == 0 || lastConnection.in == 1) && lastConnection.out == 3);
			}
		}
		// Sprawdzenie, czy w przypadku gdy pierwszy rodzic jest bardziej przystosowany to wszystkie polaczenia niedopasowane zostana przypisane dzieciom oraz dopasowane takze znajda sie w potomstwie
		TEST_METHOD(Crossover_FirstParentMoreFit_AllFirstParentInnovConnectionsInherited)
		{
			NEAT_Network baseNetwork(NEAT_Network(2, 1, 90).Mutate_AddNode());

			NEAT_Network secondParent(baseNetwork.Mutate_AddNode().Mutate_AddNode());
			NEAT_Network firstParent(baseNetwork.Mutate_AddNode());
			firstParent.fitness = 1;
			secondParent.fitness = 0;

			NEAT_Network *child1 = nullptr, *child2 = nullptr;

			NEAT_Network::Crossover(firstParent, secondParent, &child1, &child2);

			auto firstParentConnections = firstParent.GetNodesConnections();
			auto secondParentConnections = secondParent.GetNodesConnections();

			Assert::IsFalse(child1 == nullptr, (wchar_t*)("Child1 nullptr"));
			Assert::IsFalse(child2 == nullptr, (wchar_t*)("Child2 nullptr"));

			auto firstChildConnections = child1->GetNodesConnections();
			auto secondChildConnections = child2->GetNodesConnections();

			Assert::AreEqual(firstParentConnections.size(), firstChildConnections.size(), (wchar_t*)("Parent and child1 connection list sizes are different"));
			Assert::AreEqual(firstParentConnections.size(), secondChildConnections.size(), (wchar_t*)("Parent and child2 connection list sizes are different"));

			Assert::IsTrue(firstParentConnections.back() == firstChildConnections.back(), (wchar_t*)("Parent and child1 last items are different"));
			Assert::IsTrue(firstParentConnections.back() == secondChildConnections.back(), (wchar_t*)("Parent and child1 last items are different"));
		}
		// Sprawdzenie, czy w przypadku gdy drugi rodzic jest bardziej przystosowany to wszystkie polaczenia niedopasowane zostana przypisane dzieciom oraz dopasowane takze znajda sie w potomstwie
		TEST_METHOD(Crossover_SecondParentMoreFit_AllSecondParentInnovConnectionsInherited)
		{
			NEAT_Network baseNetwork(NEAT_Network(2, 1, 90).Mutate_AddNode());

			NEAT_Network secondParent(baseNetwork.Mutate_AddNode());
			NEAT_Network firstParent(baseNetwork.Mutate_AddNode().Mutate_AddNode());
			firstParent.fitness = 0;
			secondParent.fitness = 1;

			NEAT_Network *child1 = nullptr, *child2 = nullptr;

			NEAT_Network::Crossover(firstParent, secondParent, &child1, &child2);

			auto firstParentConnections = firstParent.GetNodesConnections();
			auto secondParentConnections = secondParent.GetNodesConnections();

			Assert::IsFalse(child1 == nullptr, (wchar_t*)("Child1 nullptr"));
			Assert::IsFalse(child2 == nullptr, (wchar_t*)("Child2 nullptr"));

			auto firstChildConnections = child1->GetNodesConnections();
			auto secondChildConnections = child2->GetNodesConnections();

			Assert::AreEqual(secondParentConnections.size(), firstChildConnections.size(), (wchar_t*)("Parent and child1 connection list sizes are different"));
			Assert::AreEqual(secondParentConnections.size(), secondChildConnections.size(), (wchar_t*)("Parent and child2 connection list sizes are different"));

			Assert::IsTrue(secondParentConnections.back() == firstChildConnections.back(), (wchar_t*)("Parent and child1 last items are different"));
			Assert::IsTrue(secondParentConnections.back() == secondChildConnections.back(), (wchar_t*)("Parent and child1 last items are different"));
		}
		// Sprawdzenie, czy w przypadku, gdy obaj rodzice sa rownie przystosowani, dzieci odziedzicza polaczenia po obu rodzicach
		TEST_METHOD(Crossover_BothParentsEquallyFit_ConnectionsInheritedFromBothParents)
		{
			NEAT_Network baseNetwork(NEAT_Network(2, 1, 90).Mutate_AddNode());

			NEAT_Network secondParent(baseNetwork);
			NEAT_Network firstParent(baseNetwork);
			firstParent.AddConnection(NodesConnection(0, 3, 0.0, true, 10));
			secondParent.AddConnection(NodesConnection(1, 3, 0.0, true, 20));
			firstParent.fitness = 1;
			secondParent.fitness = 1;

			NEAT_Network *child1 = nullptr, *child2 = nullptr;
			NEAT_Network::Crossover(firstParent, secondParent, &child1, &child2);

			auto firstParentConnections = firstParent.GetNodesConnections();
			auto secondParentConnections = secondParent.GetNodesConnections();

			Assert::IsFalse(child1 == nullptr);
			Assert::IsFalse(child2 == nullptr);

			auto firstChildConnections = child1->GetNodesConnections();
			auto secondChildConnections = child2->GetNodesConnections();

			auto child1Size = firstChildConnections.size();
			auto child2Size = secondChildConnections.size();
			size_t child1ExpectedSize = 6;
			size_t child2ExpectedSize = 6;

			Assert::AreEqual(child1ExpectedSize, child1Size);
			Assert::AreEqual(child2ExpectedSize, child2Size);

			Assert::IsTrue(
				(firstParentConnections.back() == firstChildConnections.back() && secondParentConnections.back() == firstChildConnections[child1Size - 2]) 
				||
				(secondParentConnections.back() == firstChildConnections.back() && firstParentConnections.back() == firstChildConnections[child1Size - 2])
			);
			Assert::IsTrue(
				(firstParentConnections.back() == secondChildConnections.back() && secondParentConnections.back() == secondChildConnections[child2Size - 2])
				||
				(secondParentConnections.back() == secondChildConnections.back() && firstParentConnections.back() == secondChildConnections[child2Size - 2])
			);
		}
		// Sprawdzenie czy prawidlowo wyliczana jest wartosc podobienstwa dwoch sieci
		TEST_METHOD(NetworksCompatibility_ExampleTwoNetworks_ExpectedValue)
		{
			// Network1: 0, 1, 2, 3,  , 5 
			// Network2: 0, 1, 2,  , 4,  

			NEAT_Network network1(90);
			NEAT_Network network2(90);
			
			network1.AddConnection(NodesConnection(0, 0, 1, true, 0));
			network1.AddConnection(NodesConnection(0, 0, 2, true, 1));
			network1.AddConnection(NodesConnection(0, 0, 3, true, 2));
			network1.AddConnection(NodesConnection(0, 0, 4, true, 3));
			network1.AddConnection(NodesConnection(0, 0, 5, true, 5));
			
			network2.AddConnection(NodesConnection(0, 0, 6, true, 4));
			network2.AddConnection(NodesConnection(0, 0, 2, true, 2));
			network2.AddConnection(NodesConnection(0, 0, -1, true, 1));
			network2.AddConnection(NodesConnection(0, 0, 3, true, 0));

			const double c1 = 1;
			const double c2 = 1;
			const double c3 = 1;
			const unsigned int N = 1;

			double expected = (1*1/1 + 1*2/1 + 1*(6/3));
			double actual = NEAT_Network::NetworksCompatibility(network1, network2, c1, c2, c3, N);

			Assert::AreEqual(expected, actual);
		}
	};
}