#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Unit_Tests
{		
	TEST_CLASS(UTests_Coordinates)
	{
	public:

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie w lewo o zadan� ilo�� krok�w
		TEST_METHOD(MoveLeft_DifferentStepValues_CoordinatesMovedToTheLeftWithStep)
		{
			// Bez kroku
			Coordinates result1 = Coordinates(0, 0).MoveLeft(0);
			// Krok domy�lny (1)
			Coordinates result2 = Coordinates(0, 0).MoveLeft();
			// Krok inny ni� domy�lny
			Coordinates result3 = Coordinates(0, 0).MoveLeft(5);
			
			Coordinates expected1(0, 0);
			Coordinates expected2(-1, 0);
			Coordinates expected3(-5, 0);

			Assert::IsTrue(result1.X == expected1.X && result1.Y == expected1.Y);
			Assert::IsTrue(result2.X == expected2.X && result2.Y == expected2.Y);
			Assert::IsTrue(result3.X == expected3.X && result3.Y == expected3.Y);
		}

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie w prwo o zadan� ilo�� krok�w
		TEST_METHOD(MoveRight_DifferentStepValues_CoordinatesMovedToTheRightWithStep)
		{
			// Bez kroku
			Coordinates result1 = Coordinates(0, 0).MoveRight(0);
			// Krok domy�lny (1)
			Coordinates result2 = Coordinates(0, 0).MoveRight();
			// Krok inny ni� domy�lny
			Coordinates result3 = Coordinates(0, 0).MoveRight(5);

			Coordinates expected1(0, 0);
			Coordinates expected2(1, 0);
			Coordinates expected3(5, 0);

			Assert::IsTrue(result1.X == expected1.X && result1.Y == expected1.Y);
			Assert::IsTrue(result2.X == expected2.X && result2.Y == expected2.Y);
			Assert::IsTrue(result3.X == expected3.X && result3.Y == expected3.Y);
		}

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie do g�ry o zadan� ilo�� krok�w
		TEST_METHOD(MoveUp_DifferentStepValues_CoordinatesMovedUpWithStep)
		{
			// Bez kroku
			Coordinates result1 = Coordinates(0, 0).MoveUp(0);
			// Krok domy�lny (1)
			Coordinates result2 = Coordinates(0, 0).MoveUp();
			// Krok inny ni� domy�lny
			Coordinates result3 = Coordinates(0, 0).MoveUp(5);

			Coordinates expected1(0, 0);
			Coordinates expected2(0, 1);
			Coordinates expected3(0, 5);

			Assert::IsTrue(result1.X == expected1.X && result1.Y == expected1.Y);
			Assert::IsTrue(result2.X == expected2.X && result2.Y == expected2.Y);
			Assert::IsTrue(result3.X == expected3.X && result3.Y == expected3.Y);
		}

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie w d� o zadan� ilo�� krok�w
		TEST_METHOD(MoveDown_DifferentStepValues_CoordinatesMovedDownWithStep)
		{
			// Bez kroku
			Coordinates result1 = Coordinates(0, 0).MoveDown(0);
			// Krok domy�lny (1)
			Coordinates result2 = Coordinates(0, 0).MoveDown();
			// Krok inny ni� domy�lny
			Coordinates result3 = Coordinates(0, 0).MoveDown(5);

			Coordinates expected1(0, 0);
			Coordinates expected2(0, -1);
			Coordinates expected3(0, -5);

			Assert::IsTrue(result1.X == expected1.X && result1.Y == expected1.Y);
			Assert::IsTrue(result2.X == expected2.X && result2.Y == expected2.Y);
			Assert::IsTrue(result3.X == expected3.X && result3.Y == expected3.Y);
		}

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie o zadany wektor
		TEST_METHOD(Move_DifferentStepValues_CoordinatesMovedByAVector)
		{
			// Bez kroku
			Coordinates result1 = Coordinates(0, 0).Move(0, 0);
			// Krok domy�lny (1)
			Coordinates result2 = Coordinates(0, 0).Move(-1, 1);
			// Krok inny ni� domy�lny
			Coordinates result3 = Coordinates(0, 0).Move(5, -5);

			Coordinates expected1(0, 0);
			Coordinates expected2(-1, 1);
			Coordinates expected3(5, -5);

			Assert::IsTrue(result1.X == expected1.X && result1.Y == expected1.Y);
			Assert::IsTrue(result2.X == expected2.X && result2.Y == expected2.Y);
			Assert::IsTrue(result3.X == expected3.X && result3.Y == expected3.Y);
		}

		// Sprawdzenie dzia�ania operatora por�wnania (obie wsp�rz�dne dw�ch koordynat�w musz� by� identyczne)
		TEST_METHOD(OperatorEquals_EqualCoordinates_ReturnsTrue)
		{
			Coordinates first(1, 1);
			Coordinates second(1, 1);

			Assert::IsTrue(first == second);
		}

		// Sprawdzenie, czy koordynaty zostan� uznane za r�ne je�eli pierwsza wsp�rz�dna jest r�na, a druga identyczna
		TEST_METHOD(OperatorEquals_DifferentXValue_ReturnsFalse)
		{
			Coordinates first(1, 1);
			Coordinates second(2, 1);

			Assert::IsFalse(first == second);
		}

		// Sprawdzenie, czy koordynaty zostan� uznane za r�ne je�eli pierwsza wsp�rz�dna jest r�na, a druga identyczna
		TEST_METHOD(OperatorEquals_DifferentYValue_ReturnsFalse)
		{
			Coordinates first(1, 1);
			Coordinates second(1, 2);

			Assert::IsFalse(first == second);
		}
	};
}