#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace Unit_Tests
{
	TEST_CLASS(UTests_GameObject)
	{
	public:

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie w lewo o zadan� ilo�� krok�w
		TEST_METHOD(MoveLeft_NonZeroValue_ObjectBodyPartsMovedCorrectly)
		{
			vector<Coordinates> resultBody;

			resultBody.push_back(Coordinates(0, 0));
			resultBody.push_back(Coordinates(0, 1));

			GameObject result(resultBody, 2, 1);
			result.MoveLeft(2);

			vector<Coordinates> expectBody;

			expectBody.push_back(Coordinates(-2, 0));
			expectBody.push_back(Coordinates(-2, 1));

			GameObject expected(expectBody, 2, 1);

			Assert::IsTrue(result.BodyParts[0] == expected.BodyParts[0] && result.BodyParts[1] == expected.BodyParts[1]);
		}

		// Sprawdzenie, czy prawid�owo nast�puje przesuni�cie w prawo o zadan� ilo�� krok�w
		TEST_METHOD(MoveRight_NonZeroValue_ObjectBodyPartsMovedCorrectly)
		{
			vector<Coordinates> resultBody;

			resultBody.push_back(Coordinates(0, 0));
			resultBody.push_back(Coordinates(0, 1));

			GameObject result(resultBody, 2, 1);
			result.MoveRight(2);

			vector<Coordinates> expectBody;

			expectBody.push_back(Coordinates(2, 0));
			expectBody.push_back(Coordinates(2, 1));

			GameObject expected(expectBody, 2, 1);

			Assert::IsTrue(result.BodyParts[0] == expected.BodyParts[0] && result.BodyParts[1] == expected.BodyParts[1]);
		}

		// Sprawdzenie, czy prawid�owo zosta�a wykryta kolizja
		TEST_METHOD(IsCollision_TheSameCoordsInOtheGameObject_True)
		{
			vector<Coordinates> firstBody;

			firstBody.push_back(Coordinates(0, 0));
			firstBody.push_back(Coordinates(0, 1));

			GameObject first(firstBody, 2, 1);

			vector<Coordinates> secondBody;

			secondBody.push_back(Coordinates(0, 1));

			GameObject second(secondBody, 2, 1);

			Assert::IsTrue(first.IsCollision(second));
		}

		// Sprawdzenie, czy prawid�owo NIE zosta�a wykryta kolizja
		TEST_METHOD(IsCollision_AllCoordsDifferent_False)
		{
			vector<Coordinates> firstBody;

			firstBody.push_back(Coordinates(0, 0));
			firstBody.push_back(Coordinates(0, 1));

			GameObject first(firstBody, 2, 1);

			vector<Coordinates> secondBody;

			secondBody.push_back(Coordinates(1, 0));

			GameObject second(secondBody, 2, 1);

			Assert::IsFalse(first.IsCollision(second));
		}
	};
}