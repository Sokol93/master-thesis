#include <iostream>
#include <string>
#include <time.h>
#include <thread>
#include <mutex>
#include "Coordinates.h"
#include "GameBoard.h"
#include "GameObject.h"
#include "NEAT_Network.h"
#include "HelperFunctions.h"


/// <summary>
/// Inicjalizacja populacji
/// </summary>
void InitPopulation(std::vector<NEAT_Network> &population, unsigned int populationSize, unsigned int inputs, unsigned int outputs, unsigned int weightPermutationProb);
/// <summary>
/// Zlecenie wylecznia jakosci ka�dego osobnika z populacji
/// </summary>
void MeasureFitness(std::vector<std::vector<NEAT_Network> > &population, const GameBoard &gameBoard);
/// <summary>
/// Wylicza wartosc jakosci/przystosowania dla kazdego osobnika z zakresu
/// Zakres <indexStart, indexEnd)
/// </summary>
void MeasureFitnessInSpieces(std::vector<NEAT_Network> &population, GameBoard gameBoard);
/// <summary>
/// Tworzy postac gracza na planszy
/// </summary>
GameObject CreatePlayer();
/// <summary>
/// Czeka na wszystkie watki, az sie zakoncza
/// </summary>
void JoinAndClearThreads(std::list<std::thread> &threads);
/// <summary>
/// Zleca kazdemu z gatunkow na wybranie odpowiedniej ilosci osobnikow do reprodukcji
/// </summary>
void SelectToReproduction(std::vector<std::vector<NEAT_Network> > &spieces, std::vector<std::vector<NEAT_Network*> > &selectedIndividuals, unsigned int percentageSelectionSize);
/// <summary>
/// Selekcja (ze zwracaniem oraz dodatkowymi elementami wyboru) osobnikow do reprodukcji
/// </summary>
std::vector<NEAT_Network*> SelectToReproductionFromSpieces(std::vector<NEAT_Network> &population, const unsigned int amountToSelect);
/// <summary>
/// Wykonanie operacji reprodukcji
/// </summary>
void Reproduction(std::vector<std::vector<NEAT_Network> > &population, std::vector<std::vector<NEAT_Network*> > selectedIndividuals, const NEAT_Network &bestIndivInGeneration, unsigned int crossoverProb, unsigned int addConnectionProb, unsigned int weightChangingProb, const double speciesTreshold, const double c1, const double c2, const double c3, const unsigned int N, unsigned int minSpeciesSizeToRewrite, unsigned int populationSize, double matingElitesProb);
/// <summary>
/// Dodanie osobnika do populacji do odpowiedniego gatunku (ewentualnie tworzac nowy). Podczas dodawania istnieje szansa na zmiane wag dziecka
/// </summary>
void AddIndividualToPopulation(std::vector<std::vector<NEAT_Network> > &population, const NEAT_Network &individual, unsigned int &newPopulationCounter, const double speciesTreshold, unsigned int weightChangingProb, const double c1, const double c2, const double c3, const unsigned int N);
/// <summary>
/// Wyszukuje i dodaje do listy najlepszego osobnika z aktualnej generacji
/// </summary>
void FindBestIndividualInGeneration(const std::vector<std::vector<NEAT_Network> > &population, std::vector<NEAT_Network> &bestIndividualsInGenerations);
/// <summary>
/// Pokazuje ruchy nalepiej przystosowanego gracza
/// </summary>
void RunBestIndividual(const std::vector<NEAT_Network> &bestIndividualsInGenerations, GameBoard gameBoard);

ActivationFunction *func = new BinaryFunction();

int main()
{
	srand((unsigned int)time(NULL));

	// Parametry i zmienne dla algorytmu NEAT
	const unsigned int maxGenerations = 1000;
	const unsigned int crossoverProb = 90;
	const unsigned int mutationProb = 100 - crossoverProb;
	const unsigned int addConnectionProb = 50;
	const unsigned int addNodeProb = 100 - addConnectionProb;
	const unsigned int weightChangingProb = 90;
	const unsigned int weightPermutationProb = 90;
	const unsigned int weightNewValueProb = 100 - weightPermutationProb;
	const unsigned int populationSize = 20;
	const unsigned int percentageSelectionSize = 50;
	const unsigned int minSpeciesSizeToRewrite = 5;
	const double matingElitesProb = 10;
	const double c1 = 1.0;
	const double c2 = 1.0;
	const double c3 = 1.0;
	const unsigned int N = 1;
	const double speciesTreshold = 10.0;
	const unsigned int maxSpeciesImproveTime = 100; // liczone w generacjach
	const unsigned int inputs = 6;
	const unsigned int outputs = 2;

	// Parametry planszy gry
	const unsigned int mapWidth = 10;
	const unsigned int mapHeight = 6;
	const unsigned int maxScore = 1000;
	const unsigned int obstackleCreationProb = 10;
	const unsigned int obstacklesMinFreeSpace = 10;
	const unsigned int obstacklesMaxFreeSpace = 10;
	const unsigned int playerMaxJumpPower = 3;
	const unsigned int playerCrouchTime = 3;

	// Inne zmienne
	const unsigned int threadsNumber = 4;

	// Do testow
	int testsCounter;
	int totalGenerations = 0;

	std::cout << "Podaj liczbe testow: ";
	std::cin >> testsCounter;


	for (int i = 0; i < testsCounter; ++i)
	{
		GameBoard gameBoard(CreatePlayer(), mapHeight, mapWidth, obstackleCreationProb, obstacklesMinFreeSpace, obstacklesMaxFreeSpace, maxScore, 2, playerMaxJumpPower, playerCrouchTime, false);
		std::vector<std::vector<NEAT_Network> > population(1); // populacja gatunkow
		std::vector<std::vector<NEAT_Network*> > selectedIndividuals;
		std::vector<NEAT_Network> bestIndividualsInGenerations;
		NEAT_Network bestIndividual(weightPermutationProb);

		unsigned int generation = 0;
		unsigned int maxFitness = 0; // najlepsza wartosc jakosci dotychczas otrzymana
		unsigned int noImprovementTime = 0; // czas (w generacjach) ile nie bylo poprawy najwiekszej dotychczasowej jakosci najlepszego osobnika

		InitPopulation(population[0], populationSize, inputs, outputs, weightPermutationProb);

		//gameBoard.SetTrainingStatus(true).SetMinFreeSpace(10).SetMaxFreeSpace(15).SetWidth(30);
		//gameBoard.StartConsoleGame();
		//return 0;

		while (generation++ < maxGenerations)
		{
			//std::cout << generation << " ";
			// Ocena
			MeasureFitness(population, gameBoard);
			//continue;
			FindBestIndividualInGeneration(population, bestIndividualsInGenerations);

			if (maxFitness < bestIndividualsInGenerations.back().fitness)
			{
				maxFitness = bestIndividualsInGenerations.back().fitness;
				bestIndividual = bestIndividualsInGenerations.back();
				noImprovementTime = 0;
				//std::cout << "#";
			}
			else
			{
				noImprovementTime += 1;
				if (noImprovementTime > maxSpeciesImproveTime)
				{
					population.erase(population.begin() + 1, population.end());
					InitPopulation(population[0], populationSize, inputs, outputs, weightPermutationProb);
					bestIndividualsInGenerations.clear();
					//generation = 0;
					noImprovementTime = 0;
					maxFitness = 0;
					continue;
				}
			}

			//std::cout << "Generation: " << generation << "\t Last best fitness: " << bestIndividualsInGenerations.back().fitness << std::endl;

			if (bestIndividualsInGenerations.back().fitness >= maxScore)
				break;

			// Selekcja
			SelectToReproduction(population, selectedIndividuals, percentageSelectionSize);

			// Reprodukcja
			Reproduction(population, selectedIndividuals, bestIndividual, crossoverProb, addConnectionProb, weightChangingProb, speciesTreshold, c1, c2, c3, N, minSpeciesSizeToRewrite, populationSize, matingElitesProb);
		}

		if (testsCounter <= 1)
		{
			int response;
			gameBoard.SetSpeed(10).SetWidth(50).SetMinFreeSpace(10).SetMaxFreeSpace(15);

			do
			{
				system("cls");

				for (int i = 0; i < bestIndividualsInGenerations.size(); ++i)
				{
					std::cout << "Generation: " << i << "\t Best indiv fitness: " << bestIndividualsInGenerations[i].fitness << std::endl;
				}

				std::cout << "Z ktorej generacji najlepszego osobnika pokazac (-1 = zakoncz program)?\t";
				std::cin >> response;

				if (response >= 0 && response < bestIndividualsInGenerations.size())
				{
					//ActivationFunction *func = new BinaryFunction();
					std::cout << gameBoard.Start(bestIndividualsInGenerations[response].ToNNetwork(func), true) << std::endl;
					//delete func;
				}

			} while (response >= 0);
		}

		//population.clear();
		//selectedIndividuals.clear();
		//bestIndividualsInGenerations.clear();
		std::cout << "Test number: " << i << std::endl;
		totalGenerations += generation;
	}

	std::cout << std::endl << std::endl << totalGenerations / testsCounter << std::endl;

	delete func;

	system("PAUSE");
	return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void InitPopulation(std::vector<NEAT_Network> &population, unsigned int populationSize, unsigned int inputs, unsigned int outputs, unsigned int weightPermutationProb)
{
	population.clear();

	for (unsigned int i = 0; i < populationSize; ++i)
	{
		population.push_back(NEAT_Network(inputs, outputs, weightPermutationProb));
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MeasureFitness(std::vector<std::vector<NEAT_Network> > &population, const GameBoard &gameBoard)
{
	//std::list<std::thread> threads;

	for (auto &spieces : population)
	{
		//threads.push_back(std::thread(MeasureFitnessInSpieces, std::ref(spieces), gameBoard));
		MeasureFitnessInSpieces(spieces, gameBoard);
	}

	//JoinAndClearThreads(threads);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MeasureFitnessInSpieces(std::vector<NEAT_Network> &species, GameBoard gameBoard)
{
	//ActivationFunction *func = new BinaryFunction();

	for (auto &individual : species)
	{
		int fitness = gameBoard.Start(individual.ToNNetwork(func));
		individual.fitness = fitness;
		gameBoard.Restart();
	}

	//delete func;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GameObject CreatePlayer()
{
	std::vector<Coordinates> player_body_parts(3);
	player_body_parts[0].Move(5, 0);
	player_body_parts[1].Move(5, 1);
	player_body_parts[2].Move(5, 2);
	return GameObject(player_body_parts, 3, 1);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void JoinAndClearThreads(std::list<std::thread> &threads)
{
	for (auto &thread : threads)
	{
		thread.join();
	}

	threads.clear();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SelectToReproduction(std::vector<std::vector<NEAT_Network> > &population, std::vector<std::vector<NEAT_Network*> > &selectedIndividuals, unsigned int percentageSelectionSize)
{
	selectedIndividuals.clear();

	for (auto &spieces : population)
	{
		selectedIndividuals.push_back(SelectToReproductionFromSpieces(spieces, spieces.size()*percentageSelectionSize / 100 + 1));
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<NEAT_Network*> SelectToReproductionFromSpieces(std::vector<NEAT_Network> &spieces, const unsigned int amountToSelect)
{
	int size = spieces.size();
	std::vector<NEAT_Network*> selectedIndividuals;
	
	// Klasyczna selekcja ruletkowa
	unsigned int fitnessSum = 0;
	std::vector<unsigned int> probabilities;
	probabilities.reserve(size);

	for (const auto &indiv : spieces)
	{
		fitnessSum += indiv.fitness;
		probabilities.push_back(indiv.fitness);
	}

	for (unsigned int i = 0; i < amountToSelect; ++i)
	{
		unsigned int value = rand() % fitnessSum + 1;
		unsigned int sum = 0;

		for (int j = 0; j < size; ++j)
		{
			sum += probabilities[j];

			if (sum  >= value /*&& std::find(selectedIndividuals.begin(), selectedIndividuals.end(), &spieces[j]) == selectedIndividuals.end()*/)
			{
				selectedIndividuals.push_back(&spieces[j]);
				//fitnessSum -= probabilities[j];
				break;
			}
		}
	}

	return selectedIndividuals;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Reproduction(std::vector<std::vector<NEAT_Network> > &population, std::vector<std::vector<NEAT_Network*> > selectedIndividuals, const NEAT_Network &bestIndivInGeneration, unsigned int crossoverProb, unsigned int addConnectionProb, unsigned int weightChangingProb, const double speciesTreshold, const double c1, const double c2, const double c3, const unsigned int N, unsigned int minSpeciesSizeToRewrite, unsigned int populationSize, double matingElitesProb)
{
	std::vector<std::vector<NEAT_Network> > newPopulation;
	unsigned int newPopulationCounter = 0;

	// Dodanie najlepszego osobnika z porzedniej generacji (ze zmiana wag)
	//AddIndividualToPopulation(newPopulation, bestIndivInGeneration, newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);

	// Przepisanie najlepszych osobnikow z wystarczajaco duzych populacji
	for (auto &species : population)
	{
		std::sort(species.begin(), species.end());

		if (species.size() >= minSpeciesSizeToRewrite)
		{
			AddIndividualToPopulation(newPopulation, species.back(), newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);
		}
	}

	// Losowanie, czy ktores najlepsze osobniki z populacji zostana sparowane
	int speciesNum = population.size();

	for (int i = 0; i < speciesNum; ++i)
	{
		int multip = 100;
		int chance = rand() % (100*multip) + 1; // Gdy wyniesie 0 to znaczy, ze zdarzenie sie spelni

		if (chance <= matingElitesProb*multip && speciesNum >= 2)
		{
			int anotherSpecies;

			while ((anotherSpecies = rand() % speciesNum) == i);

			NEAT_Network *child1 = nullptr, *child2 = nullptr;

			NEAT_Network::Crossover(population[i].back(), population[anotherSpecies].back(), &child1, &child2);
			AddIndividualToPopulation(newPopulation, *child1, newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);
			AddIndividualToPopulation(newPopulation, *child2, newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);

			delete child1;
			delete child2;
		}
	}

	// Normalna reprodukcja - kazdy gatunek wykonuje swoja (losowanie gatunku -> losowanie osobnikow)
	while (newPopulationCounter < populationSize)
	{
		unsigned int crossoverChance = rand() % 100 + 1;
		unsigned int speciesIndex = rand() % speciesNum;
		NEAT_Network *firstIndivIndex = selectedIndividuals[speciesIndex][rand() % selectedIndividuals[speciesIndex].size()];
		bool forcedMutation = false;

		if (crossoverChance <= crossoverProb && (newPopulation.size() + 2) <= populationSize && selectedIndividuals[speciesIndex].size() > 1)
		{
			NEAT_Network *secondIndivIndex;

			//while ((secondIndivIndex = selectedIndividuals[speciesIndex][rand() % selectedIndividuals[speciesIndex].size()]) == firstIndivIndex);
			if ((secondIndivIndex = selectedIndividuals[speciesIndex][rand() % selectedIndividuals[speciesIndex].size()]) == firstIndivIndex)
			{
				forcedMutation = true;
			}
			else 
			{

				NEAT_Network *child1 = nullptr, *child2 = nullptr;

				NEAT_Network::Crossover(*firstIndivIndex, *secondIndivIndex, &child1, &child2);
				AddIndividualToPopulation(newPopulation, *child1, newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);
				AddIndividualToPopulation(newPopulation, *child2, newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);

				delete child1;
				delete child2;
			}
		}
		else { forcedMutation = true; }

		if (forcedMutation)
		{
			short weightMutationChance = rand() % 100 + 1;

			if (weightMutationChance <= weightChangingProb)
			{
				AddIndividualToPopulation(newPopulation, firstIndivIndex->MutateWeights(), newPopulationCounter, speciesTreshold, 0, c1, c2, c3, N);
			}
			else
			{
				unsigned int mutationAddConnectionChance = rand() % 100 + 1;

				if (mutationAddConnectionChance <= addConnectionProb)
				{
					AddIndividualToPopulation(newPopulation, firstIndivIndex->Mutate_AddConnection(), newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);
				}
				else
				{
					AddIndividualToPopulation(newPopulation, firstIndivIndex->Mutate_AddNode(), newPopulationCounter, speciesTreshold, weightChangingProb, c1, c2, c3, N);
				}
			}
		}
	}

	population.clear();
	population.swap(newPopulation);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AddIndividualToPopulation(std::vector<std::vector<NEAT_Network> > &population, const NEAT_Network &individual, unsigned int &newPopulationCounter, const double speciesTreshold, unsigned int weightChangingProb, const double c1, const double c2, const double c3, const unsigned int N)
{
	bool addedToExistingSpieces = false;
	
	NEAT_Network offspring = individual;

	if ((unsigned)(rand() % 100 + 1) <= weightChangingProb)
	{
		//offspring = individual.MutateWeights();
	}

	offspring.ResetFitness();

	if (!population.empty())
	{
		for (auto &spieces : population)
		{
			int randomIndivIndex = rand() % spieces.size();

			if (speciesTreshold >= NEAT_Network::NetworksCompatibility(offspring, spieces[randomIndivIndex], c1, c2, c3, N))
			{
				addedToExistingSpieces = true;
				spieces.push_back(offspring);
				break;
			}
		}
	}

	if (!addedToExistingSpieces)
	{
		population.push_back(std::vector<NEAT_Network>({ offspring }));
	}

	newPopulationCounter++;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void FindBestIndividualInGeneration(const std::vector<std::vector<NEAT_Network> > &population, std::vector<NEAT_Network> &bestIndividualsInGenerations)
{
	const NEAT_Network *bestFitIndiv = nullptr;

	for (const auto &species : population)
	{
		for (const auto &indiv : species)
		{
			if (bestFitIndiv == nullptr || bestFitIndiv->fitness < indiv.fitness)
				bestFitIndiv = &indiv;
		}
	}

	bestIndividualsInGenerations.push_back(*bestFitIndiv);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RunBestIndividual(const std::vector<NEAT_Network> &bestIndividualsInGenerations, GameBoard gameBoard)
{
	//gameBoard.Start(bestIndividualsInGenerations.back().ToNNetwork(), true);
}
