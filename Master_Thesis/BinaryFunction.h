#pragma once

#include "ActivationFunction.h"

/// <summary>
/// Jednoargumentowa funkcja binarna
/// </summary>
class BinaryFunction : public ActivationFunction
{
private:
	/// <summary>
	/// Przesuniecie funkcji
	/// </summary>
	double _trans;
public:
	/// <summary>
	/// Inicjalizacja parametrow
	/// Argumenty:
	///		trans - Wartosc przesuniecia funkcji
	/// </summary>
	BinaryFunction(double trans = 0) :_trans(trans) {}

	/// <summary>
	/// Wyliczenie wartosci funkcji binarnej dla argumentu
	/// Argumenty:
	///		argValue - Wartosc przesuniecia funkcji
	/// </summary>
	double Calculate(double argValue)
	{
		return (argValue - _trans > 0) ? 1 : 0;
	}
};