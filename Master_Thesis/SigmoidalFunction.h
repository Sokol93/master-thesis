#pragma once

#include "ActivationFunction.h"
#include <algorithm>

/// <summary>
/// Jednoargumentowa funkcja binarna
/// </summary>
class SigmoidalFunction : public ActivationFunction
{
private:
	/// <summary>
	/// Przesuniecie funkcji
	/// </summary>
	double _beta;
public:
	/// <summary>
	/// Inicjalizacja parametrow
	/// Argumenty:
	///		trans - Wartosc przesuniecia funkcji
	/// </summary>
	SigmoidalFunction(double beta) : _beta(beta) {}

	/// <summary>
	/// Wyliczenie wartosci funkcji binarnej dla argumentu
	/// Argumenty:
	///		argValue - Wartosc przesuniecia funkcji
	/// </summary>
	double Calculate(double argValue)
	{
		return 1 / (1 + std::exp(-_beta*argValue));
	}
};