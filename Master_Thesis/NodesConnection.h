#pragma once

/// <summary>
/// Kodowanie genu polaczenia w algorytnie NEAT
/// </summary>
class NodesConnection
{
public:
	/// <summary>
	/// Numer neuronu (wezla) wejsciowego
	/// </summary>
	short in;
	/// <summary>
	/// Numer neuronu (wezla) wyjsciowego
	/// </summary>
	short out;
	/// <summary>
	/// Waga polaczenia miedzy neuronami
	/// </summary>
	double weight;
	/// <summary>
	/// Czy polaczenie miedzy neuronami jest aktywne
	/// </summary>
	bool isEnabled;
	/// <summary>
	/// Wspolczynnik innowacyjnosci (z algortymu NEAT)
	/// </summary>
	const int innov;

	/// <summary>
	/// Inicjalizacja wszystkich parametrow polaczenia miedzy neuronami
	/// </summary>
	NodesConnection(short inNode, short outNode, double connectionWeight, bool isConnectionEnabled, int innovationNum)
		: in(inNode), out(outNode), weight(connectionWeight), isEnabled(isConnectionEnabled), innov(innovationNum) {}

	/// <summary>
	/// Operator porownania
	/// </summary>
	inline bool operator==(const NodesConnection& arg) const
	{
		return (this->in == arg.in && this->out == arg.out && this->weight == arg.weight && this->isEnabled == arg.isEnabled && this->innov == arg.innov);
	}

	/// <summary>
	/// Operator porownania wspolczynnikow innowacji
	/// </summary>
	inline bool operator==(const int arg) const
	{
		return this->innov == arg;
	}
};