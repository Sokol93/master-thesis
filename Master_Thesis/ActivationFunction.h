#pragma once

#include <list>

/// <summary>
/// Klasa abstrakcyjna jednoargumentowej funkcji aktywacji
/// </summary>
class ActivationFunction
{
public:
	/// <summary>
	/// Wyliczenie wartosci funkcji dla argumentu
	/// Argumenty:
	///		argValue - Wartosc argumentu funkcji
	/// </summary>
	virtual double Calculate(double argValue) = 0;
};