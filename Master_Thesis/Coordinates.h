#pragma once

/// <summary>
/// Struktura zawieraj�ca koordynaty w przestrzeni 2D
/// </summary>
struct Coordinates
{
	/// <summary>
	/// Wsp�rz�dna X
	/// </summary>
	int X;
	/// <summary>
	/// Wsp�rz�dna Y
	/// </summary>
	int Y;

	/// <summary>
	/// Inicjalizacja wspolrzednych
	/// </summary>
	Coordinates(int x = 0, int y = 0) : X(x), Y(y) {}

	/// <summary>
	/// Przesuni�cie wsp�rz�dnych o zadan� ilo�� punkt�w w lewo (x - step)
	/// </summary>
	/// <return>Przesuniety punkt</return>
	Coordinates MoveLeft(int step = 1) { X -= step; return *this; }
	/// <summary>
	/// Przesuni�cie wsp�rz�dnych o zadan� ilo�� punkt�w w prawo (x + step)
	/// </summary>
	/// <return>Przesuniety punkt</return>
	Coordinates MoveRight(int step = 1) { X += step; return *this; }
	/// <summary>
	/// Przesuni�cie wsp�rz�dnych o zadan� ilo�� punkt�w do g�ry (y + step)
	/// </summary>
	/// <return>Przesuniety punkt</return>
	Coordinates MoveUp(int step = 1) { Y += step; return *this;}
	/// <summary>
	/// Przesuni�cie wsp�rz�dnych o zadan� ilo�� punkt�w w d� (y - step)
	/// </summary>
	/// <return>Przesuniety punkt</return>
	Coordinates MoveDown(int step = 1) { Y -= step; return *this; }
	/// <summary>
	/// Przesuni�cie wsp�rz�dnych o zadan� warto��
	/// </summary>
	/// <return>Przesuniety punkt</return>
	Coordinates Move(int step_x, int step_y) { X += step_x; Y += step_y; return *this; }
	/// <summary>
	/// Sprawdenie, czy wsp�rz�dne obu koordynat�w s� identyczne
	/// </summary>
	/// <return>True - wspolrzedne punktow sa identyczne, False - przeciwny wypadek</return>
	bool operator==(const Coordinates &coords) const 
	{
		return (X == coords.X && Y == coords.Y);
	}
};