#pragma once

#include "GameObject.h"
#include "HelperFunctions.h"
#include <vector>
#include <random>
#include <chrono>

/// <summary>
/// Model przeszkody na planszy
/// </summary>
class Obstackle : public GameObject
{
protected:
	int _type;
public:
	/// <summary>
	/// Konstruktor tworz�cy podany typ przeszkody (-1 = losowy typ). Przeszkoda zaczyna sie zawsze od punktu x=0
	/// </summary>
	/// <param ='type'>Typ przeszkody 0-N (-1 => losowy)</param>
	/// <param ='look'>Wyglad przeszkody na planszy</param>
	Obstackle(int type = -1, char look = '#') : _type(type)
	{
		const unsigned int typesNumber = 5;
		_look = look;
		_width = 0;

		while (_width == 0) 
		{
			switch (type) {
			case 0: {
				Type0_Straight1Floor();
				break;
			}
			case 1: {
				Type1_Straight2Floor();
				break;
			}
			case 2: {
				Type2_Straight1Air();
				break;
			}
			case 3: {
				Type3_Wide2Floor();
				break;
			}
			case 4: {
				Type4_Wide2Air();
				break;
			}
			default: {
				type = RandomIntValue(0, typesNumber - 1);
			}
			}
		}

	}
private:
	/// <summary>
	/// Zwykla jednopunktowa przeszkoda na ziemi
	/// </summary>
	void Type0_Straight1Floor()
	{
		BodyParts.clear();
		BodyParts.push_back(Coordinates(0, 0));
		_height = 1;
		_width = 1;
	}
	/// <summary>
	/// Zwykla dwupnktowa przeszkoda na ziemi (stojaca)
	/// </summary>
	void Type1_Straight2Floor()
	{
		BodyParts.clear();
		BodyParts.push_back(Coordinates(0, 0));
		BodyParts.push_back(Coordinates(0, 1));
		_height = 2;
		_width = 1;
	}
	/// <summary>
	/// Jednopunktowa przeszkoda umieszczona w powietrzu
	/// </summary>
	void Type2_Straight1Air()
	{
		BodyParts.clear();
		BodyParts.push_back(Coordinates(0, 1));
		_height = 1;
		_width = 1;
	}
	/// <summary>
	/// Dwupunktowa przeszkoda umieszczona na ziemi
	/// </summary>
	void Type3_Wide2Floor()
	{
		BodyParts.clear();
		BodyParts.push_back(Coordinates(0, 0));
		BodyParts.push_back(Coordinates(1, 0));
		_height = 1;
		_width = 2;
	}
	/// <summary>
	/// Dwupunktowa przeszkoda umieszczona na w powietrzu (do unikania dolem)
	/// </summary>
	void Type4_Wide2Air()
	{
		BodyParts.clear();
		BodyParts.push_back(Coordinates(0, 2));
		BodyParts.push_back(Coordinates(1, 2));
		_height = 1;
		_width = 2;
	}
};