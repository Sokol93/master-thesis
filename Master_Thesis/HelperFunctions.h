#pragma once

#include <random>
#include <mutex>

/// <summary>
/// Aktualny globalny wspolczynnik innowacyjnosci. Kazdy nowy gen inkrementuje ten wspolczynnik i wpisuje go do genu
/// </summary>
extern unsigned int innovationNumber;
/// <summary>
/// Ochrona aktualizacji wspolczynnika innowacji
/// </summary>
extern std::mutex innovMutex;
/// <summary>
/// Generator liczb losowych
/// </summary>
extern std::default_random_engine def_rand_generator;
/// <summary>
/// Rozklad normalny
/// </summary>
extern std::normal_distribution<double> norm_distribution;

/// <summary>
/// Mozliwe typy neuronow
/// </summary>
enum NodeType
{
	SENSOR = 1,
	HIDDEN = 2,
	OUTPUT = 3
};


/// <summary>
/// Losuje wartosc generatorem liczb z rozkladu normalnego
/// </summary>
extern double RandomNormalValue();
/// <summary>
/// Losuje wartosc generatorem liczb z rozkladu normalnego
/// </summary>
extern double RandomUniformValue(double min, double max);
/// <summary>
/// Losuje liczbe calkowita z przedzialu
/// </summary>
extern int RandomIntValue(int min, int max);