#pragma once

#include <vector>
#include "ActivationFunction.h"

class Neuron
{
protected:
	/// <summary>
	/// Informacja czy warto�� neuronu zosta�a wyliczona
	/// </summary>
	bool _hasValue;
	/// <summary>
	/// Warto�� neuronu
	/// </summary>
	double _value;
	/// <summary>
	/// Lista neuron�w wej�ciowych
	/// </summary>
	std::vector<Neuron*> _neuronsIn;
	/// <summary>
	/// Lista wag do neuron�w wej�ciowych
	/// </summary>
	std::vector<double> _weights;
	/// <summary>
	/// Funkcja aktywacji neuronu
	/// </summary>
	ActivationFunction* _activationFunc;

	Neuron() {}
public:
	/// <summary>
	/// Inicjalizacja neuronu bez zadnych polaczen
	/// </summary>
	Neuron(ActivationFunction* activationFunc) : _activationFunc(activationFunc), _hasValue(false), _value(0) {}
	/// <summary>
	/// Inicjalizacja po��cze� neuronu
	/// </summary>
	Neuron(std::vector<Neuron*>& neuronsIn, std::vector<double>& weights, ActivationFunction* activationFunc)
		: _neuronsIn(neuronsIn), _weights(weights), _activationFunc(activationFunc), _hasValue(false), _value(0) {}

	/// <summary>
	/// Resetuje warto�� neuronu (jakby nie zosta�a wyliczona) - przy kolejnym odwolaniu do wartosci zostanie ona ponownie wyliczona
	/// </summary>
	void ResetValue()
	{
		_hasValue = false;
	}
	/// <summary>
	/// Podaje warto�� wyj�ciow� neuronu (wczesniej ja wylicza jezeli jeszcze nie zostala obliczona)
	/// </summary>
	virtual double GetValue()
	{
		if (!_hasValue)
		{
			double sum = 0;
			int count = _weights.size();

			for (int i = 0; i < count; ++i)
			{
				sum += _neuronsIn[i]->GetValue() * _weights[i];
			}

			_value = _activationFunc->Calculate(sum);

			_hasValue = true;
		}

		return _value;
	}
	/// <summary>
	/// Dodaje nowe wejscie do neuronu
	/// </summary>
	void AddInput(Neuron *neuron, double weight)
	{
		_neuronsIn.push_back(neuron);
		_weights.push_back(weight);
	}
	/// <summary>
	/// Zmiana funkcji aktywacji
	/// </summary>
	void SetActivationFunction(ActivationFunction *function)
	{
		_activationFunc = function;
	}
};