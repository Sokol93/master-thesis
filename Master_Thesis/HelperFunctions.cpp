#include "HelperFunctions.h"

unsigned int innovationNumber = 1;
std::default_random_engine def_rand_generator;
std::normal_distribution<double> norm_distribution(0, 5.0);
std::mutex innovMutex;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RandomNormalValue()
{
	return norm_distribution(def_rand_generator);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RandomUniformValue(double min, double max)
{
	std::uniform_real_distribution<double> uni_distribution(min, max);

	return uni_distribution(def_rand_generator);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RandomIntValue(int min, int max)
{
	std::uniform_int_distribution<int> int_distribution(min, max);

	return int_distribution(def_rand_generator);
}

