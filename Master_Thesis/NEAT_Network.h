#pragma once

#include <vector>
#include <queue>
#include "NodesConnection.h"
#include "HelperFunctions.h"
#include "NNetwork.h"

class NEAT_Network
{
private:
	/// <summary>
	/// Chromosom kodowan wezlow. Zawiera informacje o typie neuronu (wezla). Numer elementu wektora odpowiada numerowi neuronu (wezla)
	/// </summary>
	std::vector<int> _nodesTypes;
	/// <summary>
	/// Chromosom kodowan polaczen miedzy wezlami
	/// </summary>
	std::vector<NodesConnection> _nodesConnections;
	/// <summary>
	/// Prawdopodobienstwo (w procentach) na mutacje wagie poprzez jej przeksztalcenie
	/// </summary>
	unsigned int _weightPerturbationChance;
public:
	/// <summary>
	/// Wartosc przystosowania sieci
	/// </summary>
	int fitness;

	/// <summary>
	/// Podstawowy konstruktor - do testow	
	/// </summary>
	NEAT_Network(unsigned int weightPerturabationChance) : _weightPerturbationChance(weightPerturabationChance), fitness(-1) {}
	/// <summary>
	/// Inicjalizacja sieci wezlami wejsciowymi i wyjsciowymi	
	/// </summary>
	NEAT_Network(unsigned int inputs, unsigned int outputs, unsigned int weightPerturabationChance) : _weightPerturbationChance(weightPerturabationChance), fitness(-1)
	{
		if (inputs == 0 || outputs == 0)
			throw "Bledne zdefiniowanie liczby neuronow wejsciowych/wyjsciowych!!!";

		for (unsigned int i = 0; i < inputs; ++i)
			_nodesTypes.push_back(SENSOR);

		for (unsigned int i = 0; i < outputs; ++i)
			_nodesTypes.push_back(OUTPUT);

		ConnectAllInputsToAllOutputs(inputs, outputs);
	}
	/// <summary>
	/// Konstruktor kopiujacy	
	/// </summary>
	NEAT_Network(const NEAT_Network &oryginal) : _weightPerturbationChance(oryginal._weightPerturbationChance)
	{
		Copy(oryginal, *this);
	}

	/// <summary>
	/// Mutacja zmieniajaca wszystkie wagi w sieci
	/// </summary>
	NEAT_Network MutateWeights() const
	{
		NEAT_Network mutated = *this;

		for (auto &connection : mutated._nodesConnections)
		{
			if ((unsigned)(rand() % 101) <= _weightPerturbationChance)
				mutated.Mutation_PerturbateWeight(connection);
			else
				mutated.Mutation_GetNewWeight(connection);
		}

		return mutated;
	}
	/// <summary>
	/// Mutacja dodajaca nowy wezel pomiedzy istniejace juz polaczenie
	/// </summary>
	NEAT_Network Mutate_AddNode() const 
	{
		NEAT_Network mutated = *this;

		int choosenConnectionIndex = -1;
		int size = mutated._nodesConnections.size();

		while (!mutated._nodesConnections[choosenConnectionIndex = (rand() % size)].isEnabled);

		mutated._nodesConnections[choosenConnectionIndex].isEnabled = false;

		int nodeIn = mutated._nodesConnections[choosenConnectionIndex].in;
		int nodeOut = mutated._nodesConnections[choosenConnectionIndex].out;
		int newNodeNum = mutated._nodesTypes.size();

		mutated.AddNewConnection(nodeIn, newNodeNum);
		mutated.AddNewConnection(newNodeNum, nodeOut);

		mutated._nodesTypes.push_back(HIDDEN);

		return mutated;
	}
	/// <summary>
	/// Mutacja dodajaca nowe polaczenie miedzy dwoma dotychczas niepolaczonymi neuronami (wezlami)
	/// </summary>
	NEAT_Network Mutate_AddConnection() const
	{
		NEAT_Network mutated = *this;

		int inNode, outNode;
		int size = mutated._nodesTypes.size();

		const int max_tries = 500;
		int tries = 0;

		do
		{
			inNode = rand() % size;
			outNode = rand() % size;

		} while (!mutated.isConnectionLegal(inNode, outNode) && (tries++ < max_tries));

		if (tries < max_tries)
			mutated.AddNewConnection(inNode, outNode);

		return mutated;
	}
	/// <summary>
	/// Zwraca liste polaczen sieci
	/// </summary>
	std::vector<NodesConnection> GetNodesConnections() const { return _nodesConnections; }

	/// <summary>
	/// Zmienia status polaczenia miedzy neuronami (wezlami)
	/// </summary>
	void ChangeEnabledStatus(unsigned int connectionIndex, bool isEnabled)
	{
		_nodesConnections[connectionIndex].isEnabled = isEnabled;
	}
	/// <summary>
	/// Podaje liczbe neuronow w sieci
	/// </summary>
	unsigned int GetNeuronsNumber()
	{
		return _nodesTypes.size();
	}
	/// <summary>
	/// Wykonanie operacji krzyzowania
	/// </summary>	
	static void Crossover(const NEAT_Network &parent1, const NEAT_Network &parent2, NEAT_Network **child1, NEAT_Network **child2)
	{
		const NEAT_Network *moreFit = (parent1.fitness >= parent2.fitness) ? &parent1 : &parent2;
		const NEAT_Network *lessFit = (parent1.fitness >= parent2.fitness) ? &parent2 : &parent1;
		bool bothEqualFit = parent1.fitness == parent2.fitness;

		// Przekopiowanie glownych atrybutow oraz liczby i typow neuronow parent1._nodesTypes.size() > parent2._nodesTypes.size()
		if (!bothEqualFit)
		{
			*child1 = new NEAT_Network(*moreFit);
			*child2 = new NEAT_Network(*moreFit);
		}
		else
		{
			*child1 = new NEAT_Network(parent2);
			*child2 = new NEAT_Network(parent2);
		}

		(*child1)->_nodesConnections.clear();
		(*child2)->_nodesConnections.clear();

		for (const auto &connection : moreFit->_nodesConnections)
		{	
			std::vector<NodesConnection>::const_iterator iter;

			if ((iter = std::find(lessFit->_nodesConnections.begin(), lessFit->_nodesConnections.end(), connection.innov)) != lessFit->_nodesConnections.end()) // Mniej przystosowany rodzic zawiera polaczenie o takim wspolczynniku innowacji
			{
				(*child1)->AddConnection((rand() % 2) ? connection : *iter);
				(*child2)->AddConnection((rand() % 2) ? connection : *iter);
			}
			else // Mniej przystosowany rodzic nie zawiera polaczenia o takim wspolczynniku innowacji
			{
				(*child1)->AddConnection(connection);
				(*child2)->AddConnection(connection);
			}
		}

		if (bothEqualFit)
		{
			for (const auto &connection : lessFit->_nodesConnections)
			{
				if (std::find(moreFit->_nodesConnections.begin(), moreFit->_nodesConnections.end(), connection.innov) == moreFit->_nodesConnections.end()) // Mniej przystosowany rodzic zawiera polaczenie o takim wspolczynniku innowacji
				{
					(*child1)->AddConnection(connection);
					(*child2)->AddConnection(connection);
				}
			}
		}

		(*child1)->FixNetwork();
		(*child2)->FixNetwork();
	}
	/// <summary>
	/// Dodanieokreslonego polaczenia
	/// </summary>	
	void AddConnection(const NodesConnection &connection)
	{
		_nodesConnections.push_back(connection);
	}
	/// <summary>
	/// Dodanie okreslonej ilosci neuronow ukrytych do listy neuronow i ich typow
	/// </summary>	
	void AddHiddenNodes(unsigned int number)
	{
		for (unsigned int i = 0; i < number; ++i)
			_nodesTypes.push_back(HIDDEN);
	}
	/// <summary>
	/// Przetworzenie na obiekt klasy NNetwork
	/// </summary>
	NNetwork ToNNetwork(ActivationFunction *func) const
	{
		return NNetwork(_nodesTypes, _nodesConnections, func);
	}
	/// <summary>
	/// Miara prawdopodobienstwa dwoch sieci
	/// </summary>
	static double NetworksCompatibility(const NEAT_Network& network1, const NEAT_Network& network2, double c1, double c2, double c3, unsigned int N)
	{
		int disjointInnovationStart;
		int disjointInnovationEnd;

		double W = AverageWeightDiffOfMatchingGenes(network1, network2, &disjointInnovationStart, &disjointInnovationEnd);
		int D = DisjoinGenes(network1, network2, disjointInnovationStart, disjointInnovationEnd);
		int E = ExcessGenes(network1, network2, disjointInnovationStart, disjointInnovationEnd);

		return (c1*E/N + c2*D/N + c3*W);
	}
	/// <summary>
	/// Operator mniejszosci - porownuje parametry jakosci (fitness)
	/// </summary>
	bool operator<(const NEAT_Network &arg) const
	{
		return (this->fitness < arg.fitness);
	}
	/// <summary>
	/// Operator kopiujacy
	/// </summary>
	NEAT_Network& operator=(const NEAT_Network &oryginal)
	{
		Copy(oryginal, *this);
		return *this;
	}
	/// <summary>
	/// Sprawdzenie czy neurony w polaczeniach znajduja sie takze w liscie typow neuronow, jak nie to je dodaje
	/// </summary>
	void FixNetwork()
	{
		int maxNodeNum = 0;

		for (const auto &connection : _nodesConnections)
		{
			maxNodeNum = (maxNodeNum > connection.in) ? maxNodeNum : connection.in;
			maxNodeNum = (maxNodeNum > connection.out) ? maxNodeNum : connection.out;
		}

		AddHiddenNodes(maxNodeNum - _nodesTypes.size() + 1);
	}
	/// <summary>
	/// Resetuje wartosc jakosci sieci
	/// </summary>
	void ResetFitness()
	{
		fitness = -1;
	}
private:
	/// <summary>
	/// Tworzy polaczenie miedzy kazdym wejsciem i kazdym wyjsciem
	/// </summary>
	void ConnectAllInputsToAllOutputs(unsigned int inputs, unsigned int outputs)
	{
		for (unsigned int i = 0; i < inputs; ++i)
			for (unsigned int o = 0; o < outputs; ++o)
			{
				AddNewConnection(i, inputs + o);
			}
	}
	/// <summary>
	/// Zmienia wartosc wagi polaczenia na podstawie aktualnej wagi
	/// </summary>
	void Mutation_PerturbateWeight(NodesConnection &connection)
	{
		double modifier = 1 + RandomUniformValue(-0.1, 0.1);

		connection.weight *= modifier;
	}
	/// <summary>
	/// Przypisuje nowa wage do polaczenia
	/// </summary>
	void Mutation_GetNewWeight(NodesConnection &connection)
	{
		connection.weight = RandomNormalValue();
	}

	/// <summary>
	/// Kopiowanie zawartosci jednej klasy do drugiej
	/// </summary>
	void Copy(const NEAT_Network &oryginal, NEAT_Network &copy)
	{
		copy._nodesTypes = std::vector<int>(oryginal._nodesTypes);
		copy._nodesConnections = std::vector<NodesConnection>(oryginal._nodesConnections);
		copy.fitness = oryginal.fitness;
		copy._weightPerturbationChance = oryginal._weightPerturbationChance;
	}
	/// <summary>
	/// Dodaje nowe aktywne polaczenie miedzy wskazane wezly
	/// </summary>
	void AddNewConnection(unsigned int nodeIn, unsigned int nodeOut)
	{
		innovMutex.lock();
		unsigned int innov = innovationNumber++;
		innovMutex.unlock();

		_nodesConnections.push_back(NodesConnection(nodeIn, nodeOut, RandomNormalValue(), true, innov));
	}
	/// <summary>
	/// Sprawdzenie dane polaczenie nie "zepsuje" sieci
	/// </summary>
	bool isConnectionLegal(unsigned int nodeIn, unsigned int nodeOut) const
	{
		if (nodeIn == nodeOut)
			return false;

		if (_nodesTypes[nodeIn] == OUTPUT || _nodesTypes[nodeOut] == SENSOR)
			return false;

		if (ConnectionExists(nodeIn, nodeOut))
			return false;

		std::vector<NodesConnection> connections(_nodesConnections);
		connections.push_back(NodesConnection(nodeIn, nodeOut, 0, true, 0));

		// sprawdzenie, czy siec nie jest rekurencyjna
		for (unsigned int i = 0; _nodesTypes[i] == SENSOR; ++i)
		{
			if (RecurrentPathChecker(connections, i))
				return false;
		}

		return true;
	}
	/// <summary>
	/// Rekurencyjna funkcja sprawdzajaca, czy sciezka sie nie zapetla
	/// False - nie rekurencyjna
	/// True - znaleziono punkt rekurencji
	/// </summary>
	inline bool RecurrentPathChecker(const std::vector<NodesConnection> &nodesConnections, unsigned int actualNode, std::list<unsigned int> nodesPath = std::list<unsigned int>()) const
	{
		if (std::find(nodesPath.begin(), nodesPath.end(), actualNode) != nodesPath.end())
			return true;

		nodesPath.push_back(actualNode);

		for (auto &connection : nodesConnections)
		{
			if (connection.in == actualNode)
			{
				if (RecurrentPathChecker(nodesConnections, connection.out, nodesPath))
					return true;
			}
		}

		return false;
	}
	/// <summary>
	/// Sprawdzenie, czy dane polaczenie juz istnieje, w ktora kolwiek strone
	/// </summary>
	bool ConnectionExists(unsigned int nodeIn, unsigned int nodeOut) const
	{
		for (auto &connection : _nodesConnections)
		{
			if ((connection.in == nodeIn && connection.out == nodeOut) || (connection.out == nodeIn && connection.in == nodeOut))
				return true;
		}

		return false;
	}
	/// <summary>
	/// Wyliczenie sredniej roznicy wag miedzy pasujacymi genami (wzgledem wskaznika innowacji)
	/// </summary>
	static double AverageWeightDiffOfMatchingGenes(const NEAT_Network& network1, const NEAT_Network& network2, int *disjointInnovationStart, int *disjointInnovationEnd)
	{
		double sum = 0;
		int matchingGenesCounter = 0;

		const std::vector<NodesConnection> *longer = (network1._nodesConnections.size() > network2._nodesConnections.size()) ? &network1._nodesConnections : &network2._nodesConnections;
		const std::vector<NodesConnection> *shorter = (network1._nodesConnections.size() > network2._nodesConnections.size()) ? &network2._nodesConnections : &network1._nodesConnections;

		int longerSize = longer->size();
		int shorterSize = shorter->size();

		*disjointInnovationStart = -1;
		*disjointInnovationEnd = -1;

		int maxInnov1 = -1;
		int maxInnov2 = -1;

		for (int i = 0; i < longerSize; ++i)
		{
			int innov1 = longer->at(i).innov;

			maxInnov1 = (maxInnov1 > innov1) ? maxInnov1 : innov1;

			if (i < shorterSize)
			{
				int innov2 = shorter->at(i).innov;

				maxInnov2 = (maxInnov2 > innov2) ? maxInnov2 : innov2;
				std::vector<NodesConnection>::const_iterator iter;

				if ((iter = std::find(shorter->begin(), shorter->end(), innov1)) != shorter->end())
				{
					double diff = longer->at(i).weight - iter->weight;
					sum += (diff >= 0) ? diff : -diff;
					matchingGenesCounter++;

					*disjointInnovationStart = (*disjointInnovationStart > iter->innov) ? *disjointInnovationStart : iter->innov;
				}
			}
		}

		*disjointInnovationEnd = (maxInnov1 > maxInnov2) ? maxInnov2 : maxInnov1;
		(*disjointInnovationStart)++;

		return sum / matchingGenesCounter;
	}
	/// <summary>
	/// Wyliczenie liczbych niepasujacych genow w zakresie
	/// </summary>
	static int DisjoinGenes(const NEAT_Network& network1, const NEAT_Network& network2, int disjointInnovationStart, int disjointInnovationEnd)
	{
		int counter = 0;

		for (const auto &connection : network1._nodesConnections)
		{
			if (connection.innov >= disjointInnovationStart && connection.innov <= disjointInnovationEnd)
				counter++;
		}

		for (const auto &connection : network2._nodesConnections)
		{
			if (connection.innov >= disjointInnovationStart && connection.innov <= disjointInnovationEnd)
				counter++;
		}

		return counter;
	}
	/// <summary>
	/// Wyliczenie liczbych niepasujacych genow poza zakresem
	/// </summary>
	static int ExcessGenes(const NEAT_Network& network1, const NEAT_Network& network2, int disjointInnovationStart, int disjointInnovationEnd)
	{
		int counter = 0;

		for (const auto &connection : network1._nodesConnections)
		{
			if (connection.innov > disjointInnovationEnd)
				counter++;
		}

		for (const auto &connection : network2._nodesConnections)
		{
			if (connection.innov > disjointInnovationEnd)
				counter++;
		}

		return counter;
	}
};