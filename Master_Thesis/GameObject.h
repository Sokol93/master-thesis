#pragma once

#include <vector>
#include "Coordinates.h"

/// <summary>
/// Klasa modeluje oraz obs�uguje obiekt na planszy
/// </summary>
class GameObject
{
public:
	/// <summary>
	/// Wsp�rz�dne cia�a obiektu
	/// </summary>
	std::vector<Coordinates> BodyParts;
protected:
	/// <summary>
	/// Wygl�d obiektu przedstawiany przy pomocy znaku 
	/// </summary>
	char _look;
	/// <summary>
	/// Wysoko�� obiektu
	/// </summary>
	int _height;
	/// <summary>
	/// Szeroko�� obiektu
	/// </summary>
	int _width;

public:
	GameObject() {};
	/// <summary>
	/// Konstruktor inicjalizuj�cy obiekt na planszy. Element najbardziej z lewej powinien znajdowa� si� w punkcie x=0
	/// </summary>
	/// <param ='bodyParts'>Elementy wchodzace w sklad obiektu wraz z ich wzajemnymi polozeniami</param>
	/// <param ='height'>Wysokosc obiektu</param>
	/// <param ='width'>Szerokosc obiektu</param>
	/// <param ='look'>Wyglad obiektu na planszy</param>
	GameObject(const std::vector<Coordinates> &bodyParts, int height, int width, char look = '@') : BodyParts(bodyParts), _height(height), _width(width), _look(look) {}
	/// <summary>
	/// Wygl�d obiektu przedstawiany przy pomocy znaku 
	/// </summary>
	/// <return>Wyglad obiektu</return>
	char Look() const { return _look; }
	/// <summary>
	/// Wysoko�� obiektu
	/// </summary>
	/// <return>Wysokosc obiektu</return>
	int Height() const { return _height; }
	/// <summary>
	/// Szeroko�� obiektu
	/// </summary>
	/// <return>Szserokosc obiektu</return>
	int Width() const { return _width; }
	/// <summary>
	/// Przesuni�cie obiektu o zadan� ilo�� punkt�w w lewo 	(x - step)
	/// </summary>
	void MoveLeft(int step = 1)
	{
		int size = BodyParts.size();

		for (auto &bodyPart : BodyParts)
		{
			bodyPart.MoveLeft(step);
		}
	}
	/// <summary>
	/// Przesuni�cie obiektu o zadan� ilo�� punkt�w w prawo (x + step)
	/// </summary>
	void MoveRight(int step = 1)
	{
		int size = BodyParts.size();

		for (auto &bodyPart : BodyParts)
		{
			bodyPart.MoveRight(step);
		}
	}
	/// <summary>
	/// Przesuni�cie obiektu o zadan� ilo�� punkt�w w gore (x + step)
	/// </summary>
	void MoveUp(int step = 1)
	{
		int size = BodyParts.size();

		for (auto &bodyPart : BodyParts)
		{
			bodyPart.MoveUp(step);
		}
	}
	/// <summary>
	/// Przesuni�cie obiektu o zadan� ilo�� punkt�w w dol (x + step)
	/// </summary>
	void MoveDown(int step = 1)
	{
		int size = BodyParts.size();

		for (auto &bodyPart : BodyParts)
		{
			bodyPart.MoveDown(step);
		}
	}

	/// <summary>
	/// Sprawdza, czy jest kolizja mi�dzy dwoma obiektamy (czy chocia� jeden z punkt�w zawiera identyczne wsp�rz�dne)
	/// </summary>
	/// <return>Informacja czy nastapila kolizja miedzy obiektami</return>
	bool IsCollision(GameObject &obj) const
	{
		for (auto &thisBodyPart : BodyParts)
		{
			for (auto &bodyPart : obj.BodyParts)
			{
				if (thisBodyPart == bodyPart)
					return true;
			}
		}

		return false;
	}
private:

};