#pragma once

#include <list>
#include "ActivationFunction.h"
#include "NodesConnection.h"
#include "BinaryFunction.h"
#include "SigmoidalFunction.h"
#include "InputNeuron.h"
#include "Neuron.h"

/// <summary>
/// Klasa modeluj�ca dowoln� sie� neuronow�
/// </summary>	
class NNetwork
{
private:
	/// <summary>
	/// Lista neuronow sieci. Numer elementu odpowiada numerze neuronu
	/// </summary>	
	std::vector<Neuron*> _neurons;
	/// <summary>
	/// Liczba wejsc sieci
	/// </summary>	
	unsigned int _inputs;
	/// <summary>
	/// Liczba wyjsc sieci
	/// </summary>	
	unsigned int _outputs;
	/// <summary>
	/// Funkcja aktywacji neuronow
	/// </summary>
	ActivationFunction *func;
public:
	/// <summary>
	/// Inicjalizacja sieci
	/// </summary>	
	NNetwork(const std::vector<int> &nodesTypes, const std::vector<NodesConnection> &nodesConnections, ActivationFunction *func = nullptr) : _neurons(nodesTypes.size()), _inputs(0), _outputs(0)
	{
		int neuronsNum = nodesTypes.size();

		for (int i = 0; i < neuronsNum; ++i)
		{
			if (nodesTypes[i] == SENSOR)
			{
				_inputs++;
				_neurons[i] = new InputNeuron();
			}
			else if (nodesTypes[i] == OUTPUT)
			{
				_outputs++;
				_neurons[i] = new Neuron(func);
			}
			else
			{
				_neurons[i] = new Neuron(func);
			}
		}

		for (const auto &connection : nodesConnections)
		{
			if (connection.isEnabled)
				_neurons[connection.out]->AddInput(_neurons[connection.in], connection.weight);
		}
	}
	/// <summary>
	/// Destruktor sieci
	/// </summary>	
	~NNetwork()
	{
		for (auto &neuron : _neurons)
			delete neuron;

		//delete func;
	}
	/// <summary>
	/// Wylicza wartosci wyjsc sieci na podane wejscia
	/// </summary>	
	std::vector<double> Process(const std::vector<double> &params) const
	{
		int neuronNum = 0;

		for (double value : params)
		{
			static_cast<InputNeuron*>(_neurons[neuronNum])->SetValue(value);
			neuronNum++;
		}

		std::vector<double> output;

		for (unsigned int i = 0; i < _outputs; ++i)
		{
			output.push_back(_neurons[_inputs + i]->GetValue());
		}

		return output;
	}
	/// <summary>
	/// Zmiana funkcji aktywacji
	/// </summary>	
	void ChangeActivationFunction(ActivationFunction *function)
	{
		func = function;
		
		for (auto &neuron : _neurons)
			neuron->SetActivationFunction(function);
	}
	/// <summary>
	/// Ustawia wszystkie wartosci neuronow na nieobliczone (oprocz wejsc)
	/// </summary>	
	void ResetNetwork()
	{
		int size = _neurons.size();

		for (unsigned int i = _inputs; i < size; ++i)
		{
			_neurons[i]->ResetValue();
		}
	}
private:
};