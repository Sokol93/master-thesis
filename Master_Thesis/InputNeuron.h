#pragma once

#include "Neuron.h"

/// <summary>
/// Klasa modeluj�ca pojedynczy neuron wej�ciowy sieci neuronowej (wejscie sieci neuronowej)
/// </summary>
class InputNeuron : public Neuron
{
public:
	/// <summary>
	/// Inicjalizacja wartoscia neuronu wejsciowego
	/// </summary>
	InputNeuron(double neuronValue)
	{
		_hasValue = true;
		_value = neuronValue;
	}

	/// <summary>
	/// Inicjalizacja klasy bez inicjalizacji wartosci neuronu wejsciowego
	/// </summary>
	InputNeuron()
	{
		_hasValue = false;
		_value = 0;
	}

	/// <summary>
	/// Podaje warto�� neuronu wejsciowego
	/// </summary>
	double GetValue()
	{
		if (!_hasValue)
		{
			throw std::string("Nie podano wartosci dla neuronu wejsciowego!");
		}

		return _value;
	}

	/// <summary>
	/// Ustawia wartosc neuronu wejsciowego (wejscia)
	/// </summary>
	void SetValue(double value)
	{
		_value = value;
		_hasValue = true;
	}
};