#pragma once

#include <list>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <thread>
#include <mutex>
#include <conio.h>
#include "GameObject.h"
#include "Obstackle.h"
#include "HelperFunctions.h"
#include "NNetwork.h"

/// <summary>
/// Klasa modeluje plansz� gry, ni� oraz obiekt�w w niej zawartych. Dostarcza metody zarz�dzaj�cych ni�. Plansza jest 2D i sk�ada si� z kwadratowych p�l.
/// </summary>
class GameBoard
{
private:
	/// <summary>
	/// Wysokosc planszy
	/// </summary>
	int _height;
	/// <summary>
	/// Szerokosc planszy
	/// </summary>
	int _width;
	/// <summary>
	/// Obiekt gracza
	/// </summary>
	const GameObject _player;
	/// <summary>
	/// Lista przeszkod na planszy
	/// </summary>
	std::vector<Obstackle> _obstackles;
	/// <summary>
	/// Aktualny wynik w grze
	/// </summary>
	int _score;
	/// <summary>
	/// Predkosc przesowania sie przeszkod na planszy [ilosc przesuniec na sekunde]
	/// </summary>
	int _speed;
	/// <summary>
	/// Prawdopodobie�stwo powstania nowej przeszkody [%]
	/// </summary>
	int _creationProb;
	/// <summary>
	/// Minimalna odleg�o�� pomi�dzy kolejnymi przeszkodami [l. kratek]
	/// </summary>
	int _minFreeSpace;
	/// <summary>
	/// Maksymalna odleg�o�� pomi�dzy kolejnymi przeszkodami (wtedy na pewno powstanie nowa przeszkoda) [l. kratek]
	/// </summary>
	int _maxFreeSpace;
	/// <summary>
	/// Maksymalne wzniesienie przy skoku
	/// </summary>
	int _maxJumpPower;
	/// <summary>
	/// Dlugosc kucniecia
	/// </summary>
	int _crouchTime;
	/// <summary>
	/// Maksymalny mozliwy wynik do osiagniecia
	/// </summary>
	int _maxScore;
	// NeuralNetwork _playerAlgorythm

	/// <summary>
	/// Obiekt, ktory oznacza koniec mapy
	/// </summary>
	GameObject mapEnd;
	/// <summary>
	/// Odleglos od ostatniego stworzonego obiektu
	/// </summary>
	int lastObjectCreatedSpace;
	/// <summary>
	/// Czy uruchomiono tryb treningu
	/// </summary>
	bool _training;
	/// <summary>
	/// Zmienna uzywana do treningu oznaczajaca numer nastepnej przeszkody
	/// </summary>
	int _nextObstackleType = 0;

	/// <summary>
	/// Kierunek ruchu/akcji
	/// </summary>
	enum Movement
	{
		UP = 'w',
		DOWN = 's',
		NONE = '-'
	};
	
	/// <summary>
	/// Zmienna ruchu gracza. Aktualna wysokosc po wykonaniu skoku
	/// </summary>
	int _movementHeight = 0;
	/// <summary>
	/// Zmienna ruchu gracza. Kierunek zmiany wysokosci
	/// </summary>
	int _movementVelocity = 0;
	/// <summary>
	/// Zmienna ruchu gracza. Pozostala dlugosc kucniecia
	/// </summary>
	int _movementCrouchTime = 0;
	/// <summary>
	/// Zmienna ruchu gracza. Czy aktualnie trwa kucanie
	/// </summary>
	bool _movementCrouch = false;

public:
	/// <summary>
	/// Inicjalizacja atrybutow
	/// </summary>
	GameBoard(GameObject &player, int height = 6, int width = 70, int creationProb = 10, int minFreeSpace = 5, int maxFreeSpace = 15, int maxScore = 200, int speed = 1, int maxJumpPower = 3, int crouchTime = 3, bool training = false) :
		_player(player), _height(height), _width(width), _speed(speed), _maxScore(maxScore), _creationProb(creationProb), _minFreeSpace(minFreeSpace), _maxFreeSpace(maxFreeSpace), _maxJumpPower(maxJumpPower), _crouchTime(crouchTime), _training(training)
	{
		//srand((unsigned int)time(NULL));

		for (int i = 0; i <= _height; ++i)
			mapEnd.BodyParts.push_back(Coordinates(0, i));
	}

	/// <summary>
	/// Funkcja przedstawiaj�ca gr�, w kt�r� u�ytkownik mo�e gra� na konsoli 
	/// </summary>
	/// <return>Wynik ko�cowy gry</return>
	int StartConsoleGame()
	{
		Restart();
		int points = 0;

		GameObject player = _player;

		char key = '-';

		while (!CheckCollision(3, player) && points < _maxScore)
		{
			if (_kbhit())
			{
				key = _getch();

				if (key == 'p')
					break;
			}

			MovePlayer(key, player);
			key = '-';

			MoveObstackles();

			GenerateNewObstackle();

			Display(player);

			Sleep(1000/_speed);
			points += 1;
		}

		return points;
	}
	/// <summary>
	/// Rozegranie gry przy pomocy sztucznej sieci neuronowej
	/// </summary>
	int Start(NNetwork &network, bool display = false)
	{
		Restart();
		int points = 0;
		GameObject player = _player;	

		char key = NONE;

		while (!CheckCollision(3, player) && points < _maxScore)
		{
			key = ReadMoveFromNetwork(network);

			MovePlayer(key, player);

			MoveObstackles();

			GenerateNewObstackle();

			if (display)
			{
				Display(player);
				Sleep(1000 / _speed);
			}

			points += 1;
		}

		return points;
	}
	/// <summary>
	/// Restartuje gre
	/// </summary>
	void Restart()
	{
		_obstackles.clear();
		_score = -1;
		_movementHeight = 0;
		_movementCrouch = 0;
		_movementVelocity = 0;
		_movementCrouch = false;
		_nextObstackleType = 0;
	}

	int GetHeight() const { return _height; }
	GameBoard& SetHeight(int value) { _height = value; return *this; }
	bool GetTrainingStatus() const { return _training; }
	GameBoard& SetTrainingStatus(bool value) { _training = value; return *this; }
	int GetWidth() const { return _width; }
	GameBoard& SetWidth(int value) { _width = value; return *this; }
	int GetScore() const { return _score; }
	int GetSpeed() const { return _speed; }
	GameBoard& SetSpeed(int value) { _speed = value; return *this; }
	int GetCreationProb() const { return _creationProb; }
	GameBoard& SetCreationProb(int value) { _creationProb = value; return *this; }
	int GetMinFreeSpace() const { return _minFreeSpace; }
	GameBoard& SetMinFreeSpace(int value) { _minFreeSpace = value; return *this; }
	int GetMaxFreeSpace() const { return _maxFreeSpace; }
	GameBoard& SetMaxFreeSpace(int value) { _maxFreeSpace = value; return *this; }
	int GetMaxJumpPower() const { return _maxJumpPower; }
	GameBoard& SetMaxJumpPower(int value) { _maxJumpPower = value; return *this; }
	int GetCrouchTime() const { return _crouchTime; }
	GameBoard& SetCrouchTime(int value) { _crouchTime = value; return *this; }

private:

	/// <summary>
	/// Wypisanie znaku na okreslonej pozycji
	/// Argumenty:
	/// c - znak do wypisania
	/// x - wspolrzedna szerokosci na ekranie
	/// y - wspolrzedna wysokosci na ekranie
	/// </summary>
	void WriteInXY(char c, int x, int y)
	{
		COORD pos = { (SHORT)x, (SHORT)y };
		HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleCursorPosition(output, pos);
		printf("%c", c);
	}

	/// <summary>
	/// Generuje nowa przeszkode na planszy
	/// </summary>
	void GenerateNewObstackle()
	{
		const int maxObstackleTypes = 5;
		lastObjectCreatedSpace++;

		if (((rand() % 100 + 1 <= _creationProb) && lastObjectCreatedSpace >= _minFreeSpace) || lastObjectCreatedSpace >= _maxFreeSpace)
		{
			Obstackle obstackle = Obstackle( (_training) ? ((_nextObstackleType++) % maxObstackleTypes) : -1);
			obstackle.MoveRight(_width);

			_obstackles.push_back(obstackle);
			lastObjectCreatedSpace = 0;
		}
	}

	/// <summary>
	/// Porusza obiektem gracza
	/// </summary>
	void MovePlayer(char key, GameObject &player)
	{
		if (key == UP && !_movementCrouch && _movementVelocity == 0)
		{
			_movementVelocity = 1;
			//return;
		}
		if (key == DOWN && _movementCrouchTime <= 0 && _movementVelocity == 0 && !_movementCrouch)
		{
			_movementCrouch = true;
			_movementCrouchTime = _crouchTime;
			player.BodyParts.back().MoveDown();
			//return;
		}

		if (_movementVelocity != 0)
		{
			if (_movementHeight >= _maxJumpPower)
				_movementVelocity = -1;

			_movementHeight += _movementVelocity;
			player.MoveUp(_movementVelocity);

			if (_movementHeight <= 0)
				_movementVelocity = 0;

			return;
		}

		if (_movementCrouch)
		{
			if (_movementCrouchTime <= 0)
			{
				_movementCrouch = false;
				_movementCrouchTime = 0;
				player.BodyParts.back().MoveUp();
			}

			_movementCrouchTime--;
		}
	}

	/// <summary>
	/// Porzesowa przeszkody
	/// </summary>
	void MoveObstackles()
	{
		int remove = 0;

		for (auto &obstackle : _obstackles)
		{
			obstackle.MoveLeft();
			
			if (obstackle.IsCollision(mapEnd))
				remove++;
		}

		for (int i = 0; i < remove; ++i)
		{
			_obstackles.erase(_obstackles.begin());
		}
			//_obstackles.pop_front();
	}

	/// <summary>
	/// Wyswietla stan planszy na ekranie
	/// </summary>
	void Display(GameObject &player)
	{
		system("cls");

		for (auto &part : player.BodyParts)
		{
			WriteInXY(player.Look(), part.X, _height - part.Y);
		}

		for (auto &obstackle : _obstackles)
		{
			for (auto &part : obstackle.BodyParts)
			{
				WriteInXY(obstackle.Look(), part.X, _height - part.Y);
			}
		}
	}

	/// <summary>
	/// Sprawdza czy nie nastapila kolizja z ktoryms z pierwszych przeszkod
	/// Arguments: 
	///		firstObstacklesNum - Liczba pierwszych sprawdzanych przeszkod
	/// </summary>
	/// <return>Informacja czy nastapila kolizja</return>
	bool CheckCollision(int firstObstacklesNum, GameObject &player)
	{
		int size = _obstackles.size();

		for (int i = 0; i < firstObstacklesNum && i < size; ++i)
		{
			if (player.IsCollision(_obstackles[i]))
				return true;
		}

		return false;
	}

	/// <summary>
	/// Uruchomienie sieci neuronowej i analiza jej odpowiedzi
	/// </summary>
	char ReadMoveFromNetwork(NNetwork &network)
	{
		int playerX = _player.BodyParts.back().X;
		const int minReadDistance = 2;
		const unsigned int rows = 2;
		const unsigned int columns = 3;
		std::vector<double> elementsStatus(rows*columns); // statusy elementow mapy (0 - puste pole, 1 - przeszkoda)

		std::list<Coordinates> interestingPositions;

		for (unsigned int i = 0; i < rows; ++i)
		{
			for (unsigned int j = 0; j < columns; ++j)
			{
				int x = playerX + minReadDistance + i;
				int y = j;

				for (auto &obstackle : _obstackles)
				{
					for (auto &part : obstackle.BodyParts)
					{
						if (part.X > x)
							break;

						if (part.X == x && part.Y == y)
							elementsStatus[i*rows + j] = 1.0;
					}
				}
			}
		}

		std::vector<double> response(network.Process(elementsStatus));
		network.ResetNetwork();
		char key = NONE;

		if (response[0] > 0.5)
			key = UP;
		if (response[1] > 0.5)
			key = (key == UP) ? NONE : DOWN;

		return key;
	}
};